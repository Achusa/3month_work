<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFailDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fail_data', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_bin';
            $table->string('_index');
            $table->string('_type');
            $table->string('_id');
            $table->string('_score')->nullable();
            $table->string('server_name');
            $table->string('remote');
            $table->string('route');
            $table->string('route_path');
            $table->string('request_method');
            $table->string('user');
            $table->string('http_args');
            $table->string('log_id');
            $table->string('status');
            $table->string('size');
            $table->string('referer');
            $table->string('user_agent');
            $table->timestamp('@timestamp');
            $table->string('sort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fail_data');
    }
}
