<?php

namespace App\Services;

use App\Repository\DataRepository;

class DataServices
{
    public function curlData($start, $end, $a)
    {
        $curl = new \Curl\Curl();
        $curl_data = $curl->get('http://train.rd6/?start='.$start.'&end='.$end.'&from='.$a);
        $data = $curl_data->{'response'};
        $curl->close('http://train.rd6/?start='.$start.'&end='.$end.'&from='.$a);
        $data_array = json_decode($data, true); //將json字串轉成array
        unset($curl_data);
        return $data_array;
    }

    public function saveData($arrange_data)
    {   
        $data_repository = new DataRepository();
        $data = array_chunk($arrange_data, 1000);
 
        foreach ($data as $data_chunk) {
            $result = $data_repository->check($data_chunk);
            if (empty($result)) {
                $data_repository->save($data_chunk);
            } elseif ($result['result']) {
                foreach ($result['data'] as $data) {
                    unset($data_chunk[array_search($data, $data_chunk)]);
                }
                $data_repository->save($data_chunk);
            } else {
                $data_repository->saveFail($result['data']);
            }
        }
    }

    public function arrangeData($original_data) 
    {
        $results = [];
        foreach ($original_data as $data ) {
            $result = $data; 
            $result['sort'] = implode(",", $data['sort']);
            $result = array_merge($result, $data['_source']);
            unset($result['_source']);
            $data['@timestamp'] = explode(".", $result['@timestamp'])[0];
            $time = new \DateTime($data['@timestamp']);
            $update_time = $time->format('Y-m-d H:i:s');
            $result['@timestamp'] = $update_time;
            $results[] = $result;
        }
        return $results;
    }
    
    public function cutTime($start, $end)
    {
        $start = strtotime($start);
        $end = strtotime($end);

        $start_times = [$start];
        while ($start < $end) {
            $start = $start + 30;
            if ($start<$end) {
                $start_times[] = $start;
            }
        }
        $end_times = [];
        foreach ($start_times as $value) {
            var_dump($value);
            var_dump($end);
            if ($value > $end) {
                $end_times[] = $end;
            } else {
                $end_times[] = $value + 29;
            }
        }
        var_dump($start_times);
        var_dump($end_times);
        return $times = ['start' => $start_times, 'end' => $end_times];
    }
}