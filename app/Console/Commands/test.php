<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\DataServices;
use App\Models\curlTest;
use App\Jobs\Jobtest;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test {start=2019-03-07T10:11:00} {end=2019-03-07T10:11:30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data_services = new DataServices();
        $arguments = $this->arguments();
        $start = date_create($arguments['start']);
        $end = date_create($arguments['end']);
        if ($start && $end) {
            while ($start<$end) {
                $job_test = new Jobtest($start);
                dispatch($job_test);
                $start->add(new \DateInterval('PT30S'));
            }
        } else {
            $this->error('時間格式有誤');
        }
    }
}
