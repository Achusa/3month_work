<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
     * 與模型關聯的資料表。
     *
     * @var string
     */
    protected $primaryKey = 'id';
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = ['name', 'password'];
}
