<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class failData extends Model
{
    protected $primaryKey = '_id';
    protected $table = 'curl_tests';
    protected $fillable = [
        '_index',
        '_type',
        '_id',
        '_score',
        'server_name',
        'remote',
        'route',
        'route_path',
        'request_method',
        'user',
        'http_args',
        'log_id',
        'status',
        'size',
        'referer',
        'user_agent',
        '@timestamp',
        'sort'
    ];
    public $timestamps = false;
    public $incrementing = false;
}
